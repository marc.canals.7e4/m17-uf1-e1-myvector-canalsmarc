using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace M17_UF1_E1_MyVector_CanalsMarc
{
    class Program
    {
        static void Main(string[] args)
        {
            String choice = "";
            bool sortir = false;

            while (sortir == false)
            {
                Console.WriteLine("-------------------------------------------");
                Console.WriteLine("Introdueix el número de l'exercici que vols visualitzar:");
                Console.WriteLine("-------------------------------------------");
                Console.WriteLine("[1] - Introducció");
                Console.WriteLine("[2] - MyVector");
                Console.WriteLine("[3] - VectorGame");
                Console.WriteLine("[4] - Sortir del programa \n");
                Console.Write("Opció: ");
                choice = Console.ReadLine();
                switch (choice)
                {
                    case "1":
                        Program.Exercici1();
                        break;
                    case "2":
                        Program.Exercici2();
                        break;
                    case "3":
                        Program.Exercici3();
                        break;
                    case "4":
                        Console.WriteLine("Sortint del programa...");
                        sortir = true;
                        break;
                    default:
                        Console.WriteLine("Opció Incorrecte, torna-ho a intentar");
                        break;
                }
            }
        }
        static void Exercici1()
        {
            String nom = "";

            Console.WriteLine("\n-------------------------------------------");
            Console.WriteLine("------- Has seleccionat l'exercici 1 ------");
            Console.WriteLine("-------------------------------------------");
            Console.WriteLine("------- Introdueix un nom: ----------------\n");
            Console.Write("Nom: ");
            nom = Console.ReadLine();
            Console.WriteLine("\n-------------------------------------------");
            Console.WriteLine("             Hello " + nom);
            Console.WriteLine("-------------------------------------------");
        }
        static void Exercici2()
        {
            MyVector vector = new MyVector(1, 4, 3, 6);
            Console.WriteLine("-------------------------------------------");
            Console.WriteLine(vector.ToString());
            int dist = vector.distanciaEntrePuntsVector();
            Console.WriteLine("-------------------------------------------");
            Console.WriteLine("Distancia entre els dos vectors: " + dist);
            vector.canviarDirVector();
            Console.WriteLine("-------------------------------------------");
            Console.WriteLine("Vector canviant la direcció: ");
            Console.Write(vector.ToString() + "\n");
        }
        static void Exercici3()
        {
            VectorGame vectorGame = new VectorGame();
            Console.Write("Introdueix la grandaria del array de vectors que vols: ");
            int lenght = Convert.ToInt32(Console.ReadLine());
            MyVector vector = vectorGame.RandomVectors(lenght);
            bool compareByX = false;
            Console.WriteLine("Llista ordenada dels vectors segons la distancia entre ells: ");
            vectorGame.SortVectors(vector, compareByX);
            compareByX = true;
            Console.WriteLine("------------------------------------------------------");
            Console.WriteLine("Llista ordenada dels vectors segons la distancia entre el vector i l'origen: ");
            vectorGame.SortVectors(vector, compareByX);
        }
    }
}
