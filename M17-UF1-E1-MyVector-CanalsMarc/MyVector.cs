using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace M17_UF1_E1_MyVector_CanalsMarc
{
    public class MyVector
    {
        private int cord1x;
        private int cord1y;
        private int cord2x;
        private int cord2y;

        //Constructors

        public MyVector(int c1x, int c1y, int c2x, int c2y)
        {
            this.cord1x = c1x;
            this.cord1y = c1y;
            this.cord2x = c2x;
            this.cord2y = c2y;
        }

        public MyVector()
        {
        }

        //Getters i Setters

        public int getCord1x()
        {
            return this.cord1x;
        }
        public int getCord1y()
        {
            return this.cord1y;
        }
        public int getCord2x()
        {
            return this.cord2x;
        }
        public int getCord2y()
        {
            return this.cord2y;
        }
        public void setCord1x(int c1x)
        {
            this.cord1x = c1x;
        }
        public void setCord1y(int c1y)
        {
            this.cord1y = c1y;
        }
        public void setCord2x(int c2x)
        {
            this.cord2x = c2x;
        }
        public void setCord2y(int c2y)
        {
            this.cord2y = c2y;
        }

        //Funcions

        public int distanciaEntrePuntsVector()
        {
            int distancia;
            distancia = (int)Math.Sqrt(Math.Pow((this.cord1x - this.cord2x), 2) + Math.Pow((this.cord1y - this.cord2y), 2));
            return distancia;
        }

        public void canviarDirVector()
        {
            int change;
            change = cord1x;
            cord1x = cord2x;
            cord2x = change;
            change = cord1y;
            cord1y = cord2y;
            cord2y = change;

        }
        public override string ToString()
        {
            return "Vector: {" + cord1x + "," + cord1y + "} , {" + cord2x + "," + cord2y + "}";
        }
    }
}
