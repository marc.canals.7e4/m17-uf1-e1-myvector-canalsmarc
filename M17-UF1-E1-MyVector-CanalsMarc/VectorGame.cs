using System;
using System.Collections.Generic;
using System.Text;

namespace M17_UF1_E1_MyVector_CanalsMarc
{
    class VectorGame
    {
        public MyVector[] RandomVectors(int lenghtArray) 
        {
            MyVector[] arrayVectors = new MyVector[lenghtArray];

            for (int i = 0; i < lenghtArray; i++)
            {
                int x1 = 0;
                int y1 = 0;
                int x2 = 0;
                int y2 = 0;

                for (int k = 0; k < 4; k++)
                {
                    Random random = new Random();
                    x1 = random.Next(1, 100);
                    y1 = random.Next(1, 100);
                    x2 = random.Next(1, 100);
                    y2 = random.Next(1, 100);
                    
                }
                arrayVectors[i] = new MyVector(x1, y1, x2, y2);
            }
            for (int l = 0; l < lenghtArray; l++)
            {
                Console.WriteLine(arrayVectors[l].ToString());
            }
            return arrayVectors;
        }
        public MyVector[] SortVectors(MyVector[] vectors, bool IsComparableWithAxis)
        {
            if (!IsComparableWithAxis) 
            {
                MyVector change;

                for (int i = 0; i < vectors.Length; i++) 
                {
                    for (int k = 0; k < vectors.Length; k++)
                    {
                       if (vectors[k].distanciaEntrePuntsVector() < vectors[k+1].distanciaEntrePuntsVector())
                        {
                            change = vectors[i];
                            vectors[i] = vectors[k];
                            vectors[k] = change;
                        }
                    }
                }
                Console.WriteLine("Ordenat per la distancia dels punts del vector (major fins menor)");
                foreach (MyVector vector in vectors)
                {
                    Console.WriteLine(vector);
                }

            } else
            {
                MyVector change;
                int[] pRef = new int[vectors.Length];
                int dif1, dif2;

                for (int i = 0; i < vectors.Length; i++) 
                {
                    dif1 = Math.Sqrt(Math.Pow((vectors[i].getCord1x), 2) + Math.Pow((vectors[i].getCord1y), 2));
                    dif2 = Math.Sqrt(Math.Pow((vectors[i].getCord2x), 2) + Math.Pow((vectors[i].getCord2y), 2));

                    if (dif1 < dif2)
                    {
                        pRef[i] = dif1;
                    } else
                    {
                        pRef[i] = dif2;
                    }
                }
                for (int k = 0; k < vectors.Length; k++)
                {
                    for (int l = k + 1; l < vectors.Length; l++)
                    { 
                        if(pRef[i] > pRef[j])
                        {
                            change = vectors[i];
                            vectors[i] = vectors[j];
                            vectors[j] = change;
                        }
                    }
                }
                Console.WriteLine("Ordenat per la distancia al punt 0,0 (major fins menor)");
                foreach (MyVector vector in vectors)
                {
                    Console.WriteLine(vector);
                }
            }
            return vectors;
        }
    }
}
